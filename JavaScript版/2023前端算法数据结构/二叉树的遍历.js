const root = {
    val: "A",
    left: {
      val: "B",
      left: {
        val: "D"
      },
      right: {
        val: "E"
      }
    },
    right: {
      val: "C",
      right: {
        val: "F"
      }
    }
  };
  

// 前序遍历 根 -> 左 -> 右
function preOrder(root) {
    if (!root) {
        return
    }
    console.log("现在遍历的是节点",root.val);
    preOrder(root.left)
    preOrder(root.right)
}

// 中序遍历  左 ->根-> 右
function innerOrder(root) {
    if (!root) {
        return
    }
    innerOrder(root.left)
    console.log("现在遍历的是节点",root.val);
    innerOrder(root.right)
}

// 后序遍历  左 ->右 ->根
function postOrder(root) {
    if (!root) {
        return
    }
    postOrder(root.left)
    postOrder(root.right)
    console.log("现在遍历的是节点",root.val);
}

// preOrder(root)
// innerOrder(root)
postOrder(root)
