// 1 .Map 的妙用——两数求和问题

//     描述： 给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。
//     你可以假设每种输入只会对应一个答案。但是，你不能重复利用这个数组中同样的元素。

//     示例: 给定 nums = [2, 7, 11, 15], target = 9
//     因为 nums[0] + nums[1] = 2 + 7 = 9 所以返回 [0, 1]


// 1.双重循环 复杂度O(n^2)
function sum1(nums, target) {
  let res = []
  for (let index = 0; index < nums.length; index++) {
    const temp = target - nums[index]
    const i = nums.indexOf(temp)
    if (i > -1) {
      res = [index, i]
      return res
    }
  }
}

// console.log(sum1([2, 7, 11, 15], 9));

// 2. 利用 Map 复杂度O(n)
// map.has() 复杂度是O(1)
function sum2(nums, target) {
  const map = new Map()
  let res = []
  for (let index = 0; index < nums.length; index++) {
    if (map.has(target - nums[index])) {
      res = [map.get(target - nums[index]), index]
      return res
    }
    map.set(nums[index], index)
  }
}

// console.log(sum2([2, 7, 11, 15], 9));



// 合并两个有序数组

//     描述：给你两个有序整数数组 nums1 和 nums2，请你将 nums2 合并到 nums1 中，使 nums1 成为一个有序数组。
//     说明: 初始化 nums1 和 nums2 的元素数量分别为 m 和 n 。 你可以假设 nums1 有足够的空间（空间大小大于或等于 m + n）来保存 nums2 中的元素。

//     示例: 输入:
//     nums1 = [1,2,3,0,0,0], m = 3
//     nums2 = [2,5,6], n = 3
//     输出: [1,2,2,3,5,6]


// 1. 一般解法 合并后排序 合并O(n+m), 快排O(nlogn)

// 2. 双指针

// *失败的解法
// function mergeArr(nums1, nums2) {
//   const m = nums1.length
//   const n = nums2.length
//   let p1 = m - 1
//   let p2 = n - 1
//   const ind = m + n
//   let res = []
//   for (let index = ind - 1; index >= 0; index--) {
//     if (nums1[p1] >= nums2[p2]) {
//       res[index] = nums1[p1]
//       p1--
//     } else {
//       res[index] = nums2[p2]
//       p2--
//     }
//     console.log(res,index);
//     // nums1到头了 把nums2整体合到nums1
//     if (p1 <= 0) {
//       res.splice(index, 0, ...nums1.slice(0, p2 + 1))
//       return res
//     } else {
//       // nums2到头了 不操作
//     }
//   }
// }

// console.log(mergeArr([1, 2, 3,0,0,0], [2, 5, 6]))

function mergeArr(nums1, nums2) {
  let p1 = nums1.length - 1
  let p2 = nums2.length - 1
  let index = nums1.length + nums2.length - 1

  while (p2 >= 0 && p1 >= 0) {
    if (nums1[p1] >= nums2[p2]) {
      nums1[index] = nums1[p1]
      p1--
    } else {
      nums1[index] = nums2[p2]
      p2--
    }
    index--
  }

  // 将剩余的nums2元素追加到nums1末尾
  while (p2 >= 0) {
    nums1[index] = nums2[p2]
    p2--
    index--
  }

  return nums1
}

// console.log(mergeArr([1, 2, 3], [2, 5, 6]))


// /**
//  * @param {number[]} nums1
//  * @param {number} m
//  * @param {number[]} nums2
//  * @param {number} n
// //  * @return {void} Do not return anything, modify nums1 in-place instead.
//  */
// const merge = function (nums1, m, nums2, n) {
//   // 初始化两个指针的指向，初始化 nums1 尾部索引k
//   let i = m - 1, j = n - 1, k = m + n - 1
//   // 当两个数组都没遍历完时，指针同步移动
//   while (i >= 0 && j >= 0) {
//     // 取较大的值，从末尾往前填补
//     if (nums1[i] >= nums2[j]) {
//       nums1[k] = nums1[i]
//       i--
//       k--
//     } else {
//       nums1[k] = nums2[j]
//       j--
//       k--
//     }
//   }

//   // nums2 留下的情况，特殊处理一下 
//   while (j >= 0) {
//     nums1[k] = nums2[j]
//     k--
//     j--
//   }
//   console.log(nums1);
// };
// merge([1, 2, 3, 0, 0, 0], 3, [2, 5, 6], 3)




// 描述：给你一个包含 n 个整数的数组 nums，判断 nums 中是否存在三个元素 a，b，c ，使得 a + b + c = 0 ？请你找出所有满足条件且不重复的三元组。
// 注意：答案中不可以包含重复的三元组。

// 示例： 给定数组 nums = [-1, 0, 1, 2, -1, -4]， 满足要求的三元组集合为： [ [-1, 0, 1], [-1, -1, 2] ]

// * 会有数组元素重复问题
function sum3(nums) {
  let i = 0
  let j = 1
  const res = []
  const cacheMap = new Map() //缓存
  while (i < nums.length) {
    while (j < nums.length) {
      let temp = nums[i] + nums[j]
      if (cacheMap.has(temp)) {
        j++
        continue
      } else {
        cacheMap.set(temp, j)
        let k = nums.indexOf(-temp)
        if (k > -1 && k!==i && k!==j) {
          res.push([nums[i], nums[j], nums[k]])
          console.log(i,j,k);
        }
        j++
      }
    }
    i++
    j = i + 1
  }
  return res
}


console.log(sum3([-1, 0, 1, 2, -1, -4]));


